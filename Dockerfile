FROM adoptopenjdk/openjdk11:latest
VOLUME /tmp
ARG JAR_FILE=build/libs/*.jar
COPY   ${JAR_FILE} app2.jar
ENTRYPOINT ["java", "-jar", "/app2.jar"]